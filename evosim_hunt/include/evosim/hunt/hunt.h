#pragma once

//framework
#include "diversity/framework/system_manager.h"
#include "diversity/framework/entity_manager.h"
#include "diversity/framework/event_service.h"

//math
#include "diversity/math/vector_2d.h"

//neural
#include "diversity/neural/network.h"

//evosim
#include "evosim/common/team_id.h"
#include "evosim/common/sprite_cache.h"
#include "evosim/common/texture_cache.h"
#include "evosim/common/text_cache.h"

namespace evosim
{
   namespace hunt
   {
      typedef std::map<common::TeamId, sf::Color> TeamColors;

      class Hunt
      {
      public:
         Hunt(
            std::vector<::diversity::neural::Network>::const_iterator begin,
            std::vector<::diversity::neural::Network>::const_iterator end,
            bool gui);

         std::vector<float> run();

      private:
         ::diversity::framework::EntityManager m_entityManager;
         ::diversity::framework::SystemManager m_systemManager;
         ::diversity::framework::EventService m_eventService;
         common::SpriteCache m_spriteCache;
         common::TextureCache m_textureCache;
         common::TextCache m_textCache;
         TeamColors m_teamColors;
         std::vector<::diversity::framework::EntityId> m_hunterIds;

         bool const m_gui;
         int64_t const m_maxGameDuration;

         float score(::diversity::framework::EntityId);
      };
   }
}