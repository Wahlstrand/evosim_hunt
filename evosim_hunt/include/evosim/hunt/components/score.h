#pragma once

#include "diversity/framework/icomponent.h"

#include "evosim/hunt/component_types.h"

namespace evosim
{
   namespace hunt
   {
      struct Score : public ::diversity::framework::IComponent
      {
         static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::score; }

         Score();

         float m_accumulatedDistanceToNemesis;
         uint16_t m_numMisses;
         uint16_t m_numHits;
      };
   }
}