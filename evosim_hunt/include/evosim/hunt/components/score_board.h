#pragma once

#include "diversity/framework/icomponent.h"

#include "evosim/common/font_id.h"

#include "evosim/hunt/component_types.h"

namespace evosim
{
   namespace hunt
   {
      struct ScoreBoard : public ::diversity::framework::IComponent
      {
         static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::score_board; }

         ScoreBoard(common::FontId fontId);

         common::FontId const m_fontId;
         float const m_topLeftX;// = 40.0f;
         float const m_topLeftY;// = 40.0f;

         float const m_hitOffsetX; //= 40
         float const m_missOffsetX; //= 120
         float const m_deltaY; //=30
      };
   }
}