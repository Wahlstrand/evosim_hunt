#pragma once

#include "diversity/framework/icomponent.h"

#include "diversity/neural/network.h"

#include "evosim/hunt/component_types.h"

namespace evosim
{
   namespace hunt
   {
      struct Brain : public ::diversity::framework::IComponent
      {
         static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::brain; }

         Brain();
         Brain(::diversity::neural::Network&& network);

         ::diversity::neural::Network m_network;
      };
   }
}