#pragma once

#include "diversity/framework/icomponent.h"

#include "evosim/hunt/component_types.h"

namespace evosim
{
   namespace hunt
   {
      struct SineControl : public ::diversity::framework::IComponent
      {
         static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::sine_control; }
      };
   }
}