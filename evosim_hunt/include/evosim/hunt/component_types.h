#pragma once

#include "diversity/framework/icomponent_fwd.h"

namespace evosim
{
   namespace hunt
   {
      enum ComponentType : ::diversity::framework::ComponentTypeId
      {
         brain = 1000,
         score,
         sine_control,
         score_board,
      };
   }
}