#pragma once

#include "diversity/framework/isystem.h"
#include "diversity/framework/entity_manager_fwd.h"
#include "diversity/framework/component_index_fwd.h"
#include "diversity/framework/entity_tracker.h"
#include "diversity/framework/unique_entity_tracker.h"

#include "diversity/math/vector_2d.h"

#include "evosim/common/components/tick_counter.h"
#include "evosim/common/components/fixed_wing_controls.h"

#include "evosim/hunt/components/sine_control.h"

namespace evosim
{
   namespace hunt
   {
      class UpdateSineControl : public ::diversity::framework::ISystem
      {
      public:
         UpdateSineControl(
            ::diversity::framework::EntityManager& entityManager);

         //ISystem
         void update() override;

      private:
         ::diversity::framework::EntityManager& m_entityManager;
         ::diversity::framework::ComponentIndex& m_componentIndex;
         ::diversity::framework::UniqueEntityTracker<common::TickCounter> m_tickTracker;
         ::diversity::framework::EntityTracker<SineControl, common::FixedWingControls> m_entities;
      };
   }
}