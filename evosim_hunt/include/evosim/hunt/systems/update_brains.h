#pragma once

#include <vector>

#include "diversity/framework/isystem.h"
#include "diversity/framework/entity_fwd.h"
#include "diversity/framework/entity_manager_fwd.h"
#include "diversity/framework/component_index_fwd.h"
#include "diversity/framework/entity_tracker.h"

#include "diversity/math/vector_2d.h"

#include "evosim/common/components/nemesis.h"
#include "evosim/common/components/fixed_wing_controls.h"
#include "evosim/common/components/fire_controls.h"

#include "evosim/hunt/components/brain.h"

namespace evosim
{
   namespace hunt
   {
      class UpdateBrains : public ::diversity::framework::ISystem
      {
      public:
         UpdateBrains(
            ::diversity::framework::EntityManager& entityManager);

         //ISystem
         void update() override;

      private:
         ::diversity::framework::EntityManager& m_entityManager;
         ::diversity::framework::ComponentIndex& m_componentIndex;
         ::diversity::framework::EntityTracker<Brain, common::Nemesis, common::FireControls, common::FixedWingControls> m_tracker;

         void update(
            ::diversity::framework::EntityId const& pilotId,
            ::diversity::framework::EntityId const& enemyId);
      };
   }
}