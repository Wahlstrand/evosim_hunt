#pragma once

#include "diversity/framework/isystem.h"
#include "diversity/framework/entity_manager_fwd.h"
#include "diversity/framework/event_service_fwd.h"
#include "diversity/framework/component_index_fwd.h"
#include "diversity/framework/entity_tracker.h"
#include "diversity/framework/unique_entity_tracker.h"

#include "diversity/math/vector_2d.h"

#include "evosim/common/components/tick_counter.h"
#include "evosim/common/components/nemesis.h"
#include "evosim/common/components/transform.h"
#include "evosim/common/components/warhead.h"
#include "evosim/common/components/tick_limit.h"
#include "evosim/common/components/launched.h"

#include "evosim/hunt/components/score.h"

namespace evosim
{
   namespace hunt
   {
      class UpdateScore : public ::diversity::framework::ISystem
      {
      public:
         UpdateScore(
            ::diversity::framework::EntityManager& entityManager,
            ::diversity::framework::EventService& eventService);

         //ISystem
         void update() override;

      private:
         ::diversity::framework::EntityManager& m_entityManager;
         ::diversity::framework::EventService& m_eventService;
         ::diversity::framework::ComponentIndex& m_componentIndex;
         ::diversity::framework::UniqueEntityTracker<common::TickCounter> m_tickTracker;
         ::diversity::framework::EntityTracker<Score, common::Transform, common::Nemesis> m_hunters;
         ::diversity::framework::EntityTracker<common::Warhead, 
                                               common::TickLimit,
                                               common::Launched> m_missileTracker;

         void updateDistance(::diversity::framework::EntityId);
         void updateAccuracy();
      };
   }
}