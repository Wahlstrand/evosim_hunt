#pragma once

#include <vector>

#include "diversity/framework/isystem.h"
#include "diversity/framework/entity_manager_fwd.h"
#include "diversity/framework/component_index_fwd.h"
#include "diversity/framework/event_service_fwd.h"
#include "diversity/framework/unique_entity_tracker.h"

#include "evosim/common/components/tick_counter.h"

namespace evosim
{
   namespace hunt
   {
      class CheckGameOver : public ::diversity::framework::ISystem
      {
      public:
         CheckGameOver(
            ::diversity::framework::EntityManager& entityManager,
            ::diversity::framework::EventService& eventService,
            int64_t maxGameDuration);

         //ISystem
         void update() override;

      private:
         ::diversity::framework::EntityManager& m_entityManager;
         ::diversity::framework::ComponentIndex& m_componentIndex;
         ::diversity::framework::EventService& m_eventService;
         ::diversity::framework::UniqueEntityTracker<common::TickCounter> m_tickTracker;
         int64_t m_maxGameDuration;
      };
   }
}