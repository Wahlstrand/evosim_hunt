#pragma once

#include <memory>

#include <SFML/Window.hpp>

#include "diversity/framework/isystem.h"
#include "diversity/framework/entity_manager_fwd.h"
#include "diversity/framework/entity_tracker.h"
#include "diversity/framework/optional_entity_tracker.h"

#include "evosim/common/text_cache.h"
#include "evosim/common/components/label.h"

#include "evosim/hunt/components/score_board.h"
#include "evosim/hunt/components/score.h"

namespace evosim
{
   namespace hunt
   {
      class RenderHud : public ::diversity::framework::ISystem
      {
      public:
         RenderHud(
            ::diversity::framework::EntityManager& entityManager,
            ::std::shared_ptr<::sf::RenderWindow> const& spWindow,
            common::TextCache const& textCache);

         //ISystem
         void update() override;

      private:
         ::diversity::framework::EntityManager& m_entityManager;
         ::diversity::framework::ComponentIndex& m_componentIndex;
         ::std::shared_ptr<::sf::RenderWindow> m_spWindow;
         common::TextCache const& m_textCache;
         ::diversity::framework::EntityTracker<Score, common::Label> m_scoreTracker;
         ::diversity::framework::OptionalEntityTracker<ScoreBoard> m_scoreboardTracker;
      };
   }
}