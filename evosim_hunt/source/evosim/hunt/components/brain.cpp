#include "evosim/hunt/components/brain.h"

using namespace ::diversity;
using namespace ::evosim::hunt;

using namespace diversity;

Brain::Brain()
   : m_network()
{
}

Brain::Brain(neural::Network&& network)
   : m_network(std::move(network))
{
}

