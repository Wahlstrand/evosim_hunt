#include "evosim/hunt/components/score_board.h"

using namespace ::evosim;
using namespace ::evosim::hunt;

ScoreBoard::ScoreBoard(common::FontId fontId)
   : m_fontId(fontId)
   , m_topLeftX(10.0f)
   , m_topLeftY(10.0f)
   , m_hitOffsetX(80.0f)
   , m_missOffsetX(120.0f)
   , m_deltaY(20.0f)
{

}