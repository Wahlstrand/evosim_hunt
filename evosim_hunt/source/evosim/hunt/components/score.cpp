#include "evosim/hunt/components/score.h"

using namespace ::evosim::hunt;

Score::Score()
   : m_accumulatedDistanceToNemesis()
   , m_numMisses(0)
   , m_numHits(0)
{
}