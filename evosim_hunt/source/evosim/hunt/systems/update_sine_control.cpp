#include "evosim/hunt/systems/update_sine_control.h"

#include <cmath>

using namespace ::diversity;
using namespace ::evosim::hunt;

UpdateSineControl::UpdateSineControl(
   framework::EntityManager& entityManager)
   : m_entityManager(entityManager)
   , m_componentIndex(entityManager.componentIndex())
   , m_tickTracker(entityManager)
   , m_entities(entityManager)
{
}

void UpdateSineControl::update()
{
   auto pTick = m_componentIndex.getComponent<common::TickCounter>(m_tickTracker.track());
   float turn =
      0.5f * std::sin(0.001f * pTick->m_tick) +
      0.2f * std::sin(0.005f * pTick->m_tick) +
      0.1f * std::sin(0.01f * pTick->m_tick);

   for (auto&& entityId : m_entities.trackList())
   {
      auto pControls = m_componentIndex.getComponent<common::FixedWingControls>(entityId);
      pControls->m_turn = turn;
   }
}
