#include "evosim/hunt/systems/check_game_over.h"

#include "diversity/framework/entity_manager.h"
#include "diversity/framework/component_index.h"
#include "diversity/framework/event_service.h"

#include "evosim/common/game_events.h"

using namespace ::diversity;
using namespace ::evosim::hunt;

CheckGameOver::CheckGameOver(
   framework::EntityManager& entityManager,
   framework::EventService& eventService,
   int64_t maxGameDuration)
   : m_entityManager(entityManager)
   , m_componentIndex(entityManager.componentIndex())
   , m_eventService(eventService)
   , m_tickTracker(entityManager)
   , m_maxGameDuration(maxGameDuration)
{

}

void CheckGameOver::update()
{
   auto pTick = m_componentIndex.getComponent<common::TickCounter>(m_tickTracker.track());
   if (pTick->m_tick >= m_maxGameDuration)
   {
      m_eventService.publish(common::EvGameOver());
      return;
   }
}
