#include "evosim/hunt/systems/render_hud.h"

#include "diversity/framework/entity_manager.h"
#include "diversity/framework/component_index.h"

using namespace ::diversity;
using namespace ::evosim;
using namespace ::evosim::hunt;

RenderHud::RenderHud(
   framework::EntityManager& entityManager, 
   ::std::shared_ptr<::sf::RenderWindow> const& spWindow,
   common::TextCache const& textCache)
   : m_entityManager(entityManager)
   , m_componentIndex(entityManager.componentIndex())
   , m_spWindow(spWindow)
   , m_textCache(textCache)
   , m_scoreTracker(entityManager)
   , m_scoreboardTracker(entityManager)
{
}

void RenderHud::update()
{
   auto scoreBoardId = m_scoreboardTracker.track();
   if (scoreBoardId != framework::EntityManager::entityIdNone)
   {
      m_spWindow->setView(m_spWindow->getDefaultView());

      auto pScoreBoard = m_componentIndex.getComponent<ScoreBoard>(scoreBoardId);

      auto spSflmText = m_textCache.get(pScoreBoard->m_fontId);
      spSflmText->setString("Score");
      spSflmText->setOrigin(0.0f, 0.0f);
      spSflmText->setScale(0.8f, 0.8f);
      spSflmText->setStyle(::sf::Text::Bold);
      spSflmText->setPosition(pScoreBoard->m_topLeftX,
                              pScoreBoard->m_topLeftY);

      m_spWindow->draw(*spSflmText);

      size_t row = 1;
      spSflmText->setStyle(::sf::Text::Regular);
      for (auto scoreId : m_scoreTracker.trackList())
      {
         auto pScore = m_componentIndex.getComponent<Score>(scoreId);
         auto pLabel = m_componentIndex.getComponent<common::Label>(scoreId);

         spSflmText->setString(pLabel->m_text + ":");
         spSflmText->setPosition(pScoreBoard->m_topLeftX,  
                                 pScoreBoard->m_topLeftY + row * pScoreBoard->m_deltaY);
         m_spWindow->draw(*spSflmText);
               
         spSflmText->setString("+" + std::to_string(pScore->m_numHits));
         spSflmText->setPosition(pScoreBoard->m_topLeftX + pScoreBoard->m_hitOffsetX,
                                 pScoreBoard->m_topLeftY + row * pScoreBoard->m_deltaY);
         m_spWindow->draw(*spSflmText);

         spSflmText->setString("-" + std::to_string(pScore->m_numMisses));
         spSflmText->setPosition(pScoreBoard->m_topLeftX + pScoreBoard->m_missOffsetX,
                                 pScoreBoard->m_topLeftY + row * pScoreBoard->m_deltaY);
         m_spWindow->draw(*spSflmText);

         row++;
      }
   }
}