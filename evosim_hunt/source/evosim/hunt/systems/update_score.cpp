#include "evosim/hunt/systems/update_score.h"

#include <algorithm>
#include <iostream>

#include "diversity/framework/entity_manager.h"
#include "diversity/framework/event_service.h"
#include "diversity/framework/component_index.h"

#include "diversity/math/vector_algebra.h"

#include "evosim/common/components/life.h"
#include "evosim/common/game_events.h"

using namespace ::diversity;
using namespace ::evosim::hunt;

UpdateScore::UpdateScore(
   framework::EntityManager& entityManager, 
   framework::EventService& eventService)
   : m_entityManager(entityManager)
   , m_eventService(eventService)
   , m_componentIndex(entityManager.componentIndex())
   , m_tickTracker(entityManager)
   , m_hunters(entityManager)
   , m_missileTracker(entityManager)
{
}

void UpdateScore::update()
{
   for (auto&& scoredEntityId : m_hunters.trackList())
   {
      updateDistance(scoredEntityId);
   }
   updateAccuracy();
}

void UpdateScore::updateDistance(framework::EntityId scoredEntityId)
{
   auto pScore = m_componentIndex.getComponent<Score>(scoredEntityId);
   auto pTransform = m_componentIndex.getComponent<common::Transform>(scoredEntityId);

   auto pNemesis = m_componentIndex.getComponent<common::Nemesis>(scoredEntityId);
   auto pNemesisTransform = m_componentIndex.getComponent<common::Transform>(pNemesis->m_nemesisId);

   pScore->m_accumulatedDistanceToNemesis += math::distance(pTransform->m_pos, pNemesisTransform->m_pos);
}
void UpdateScore::updateAccuracy()
{
   for (auto&& missileId : m_missileTracker.trackList())
   {
      auto pWarhead = m_componentIndex.getComponent<common::Warhead>(missileId);
      if (pWarhead->m_exploding)
      {
         auto pLaunched = m_componentIndex.getComponent<common::Launched>(missileId);
         auto pScore = m_componentIndex.getComponent<Score>(pLaunched->m_launcherId);
         if(pScore == nullptr)
         {
            throw std::runtime_error("Missile launcher expected to have score component!");
         }
         
         pScore->m_numHits++;
         continue;
      }

      auto pTickLimit = m_componentIndex.getComponent<common::TickLimit>(missileId);
      if (pTickLimit->m_expired)
      {
         auto pLaunched = m_componentIndex.getComponent<common::Launched>(missileId);
         auto pScore = m_componentIndex.getComponent<Score>(pLaunched->m_launcherId);
         if (pScore == nullptr)
         {
            throw std::runtime_error("Missile launcher expected to have score component!");
         }

         pScore->m_numMisses++;
         continue;
      }
   }
}