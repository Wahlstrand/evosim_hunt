#include "evosim/hunt/systems/update_brains.h"

#include "diversity/framework/entity_manager.h"
#include "diversity/framework/component_index.h"

#include "diversity/math/math.h"
#include "diversity/math/vector_2d.h"
#include "diversity/math/vector_algebra.h"

#include "diversity/neural/network.h"

#include "evosim/common/components/transform.h"
#include "evosim/common/components/fixed_wing_controls.h"
#include "evosim/common/components/fire_controls.h"

#include "evosim/hunt/components/brain.h"


using namespace ::diversity;
using namespace ::evosim::hunt;

namespace
{
   template<typename T>
   T linearClamp(T in, T minIn, T maxIn, T minOut, T maxOut)
   {
      if (in > maxIn)
      {
         return maxOut;
      }
      else if (in < minIn)
      {
         return minOut;
      }
      else
      {
         return  (in - minIn) / (maxIn - minIn) * (maxOut - minOut) + minOut;
      }
   }

   //angle 0   -> 2 pi
   //out   -pi -> pi
   template <typename T>
   T toDirAngle(T angle)
   {
      return angle - math::pi();
   }
}

UpdateBrains::UpdateBrains(framework::EntityManager& entityManager)
   : m_entityManager(entityManager)
   , m_componentIndex(entityManager.componentIndex())
   , m_tracker(entityManager)
{

}

void UpdateBrains::update()
{
   for (auto&& pilotId : m_tracker.trackList())
   {
      auto pNemesis = m_componentIndex.getComponent<common::Nemesis>(pilotId);
      update(pilotId, pNemesis->m_nemesisId);
   }
}

void UpdateBrains::update(framework::EntityId const& pilotId, framework::EntityId const& enemyId)
{
   //Collect inputs
   auto pPilotTransform = m_componentIndex.getComponent<common::Transform>(pilotId);
   auto pOpponentTransform = m_componentIndex.getComponent<common::Transform>(enemyId);

   auto pilotToOpponentWorld = pOpponentTransform->m_pos - pPilotTransform->m_pos;
   auto pilotToOpponentBody = math::rotate(pilotToOpponentWorld, -1.0f * pPilotTransform->m_phi);
   auto opponentToPilotWorld = -1.0f * pilotToOpponentWorld;
   auto opponentToPilotBody = math::rotate(pilotToOpponentWorld, -1.0f * pOpponentTransform->m_phi);

   float distanceToEnemy = math::norm(pilotToOpponentWorld);

   float angleToEnemy = std::atan2(pilotToOpponentBody.m_y, pilotToOpponentBody.m_x); //-pi -> pi
   float angleFromEnemy = std::atan2(opponentToPilotBody.m_y, opponentToPilotBody.m_x); //-pi -> pi

   //Rescale inputs
   float const minActivation = -3.0f;
   float const maxActivatn = -minActivation;

   //distance 0   = min
   //distance 500 = max  
   float distanceToEnemyInput = linearClamp(distanceToEnemy, 0.0f, 500.0f, minActivation, maxActivatn);

   //angles
   //-pi = min
   //+pi = max
   float angleToEnemyInput = linearClamp(angleToEnemy, -math::pi(), math::pi(), minActivation, maxActivatn);
   float angleFromEnemyInput = linearClamp(angleFromEnemy, -math::pi(), math::pi(), minActivation, maxActivatn);

   //Stimulate network
   auto pPilotBrain = m_componentIndex.getComponent<Brain>(pilotId);
   neural::update(pPilotBrain->m_network, { distanceToEnemyInput, angleToEnemyInput, angleFromEnemyInput });

   //Collect outputs
   auto lastLayerItr = pPilotBrain->m_network.m_layers.rbegin();
   if (lastLayerItr->m_neurons.size() != 2)
   {
      throw std::runtime_error("Output layer mismatches expected output size '2'");
   }

   float turnActivation = lastLayerItr->m_neurons[0].m_activation;
   float fireActivation = lastLayerItr->m_neurons[1].m_activation;

   //Apply outputs
   //float turn = 2.0f * turnActivation - 1.0f;
   float turn = turnActivation;
   bool fire = fireActivation > 0.5;
   m_componentIndex.getComponent<common::FixedWingControls>(pilotId)->m_turn = turn;
   m_componentIndex.getComponent<common::FireControls>(pilotId)->m_fire = fire;
}