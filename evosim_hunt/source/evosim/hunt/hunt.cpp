#include "evosim/hunt/hunt.h"

//std
#include <thread>
#include <iostream>

//math
#include "diversity/math/math.h"

//framework
#include "diversity/framework/ievent_listener.h"

//components
#include "evosim/common/components/tick_counter.h"
#include "evosim/common/components/transform.h"
#include "evosim/common/components/collision.h"
#include "evosim/common/components/fixed_wing_body.h"
#include "evosim/common/components/fixed_wing_controls.h"
#include "evosim/common/components/fire_controls.h"
#include "evosim/common/components/sprite.h"
#include "evosim/common/components/background.h"
#include "evosim/common/components/screen_focus.h"
#include "evosim/common/components/team.h"
#include "evosim/common/components/nemesis.h"

#include "evosim/hunt/components/brain.h"
#include "evosim/hunt/components/score.h"
#include "evosim/hunt/components/sine_control.h"
#include "evosim/hunt/components/score_board.h"

//systems
#include "evosim/common/systems/tick.h"
#include "evosim/common/systems/update_fixed_wings.h"
#include "evosim/common/systems/update_fire_controls.h"
#include "evosim/common/systems/update_tick_limits.h"
#include "evosim/common/systems/update_collisions.h"
#include "evosim/common/systems/update_warheads.h"
#include "evosim/common/systems/clear_window.h"
#include "evosim/common/systems/process_window_events.h"
#include "evosim/common/systems/render.h"
#include "evosim/common/systems/flush_window.h"
#include "evosim/common/systems/cleanup.h"

#include "evosim/hunt/systems/check_game_over.h"
#include "evosim/hunt/systems/update_score.h"
#include "evosim/hunt/systems/update_sine_control.h"
#include "evosim/hunt/systems/update_brains.h"
#include "evosim/hunt/systems/render_hud.h"

//other
#include "evosim/common/game_event_listener.h"

using namespace ::diversity;
using namespace ::evosim::common;
using namespace ::evosim::hunt;

namespace {
   enum Sprites : SpriteId
   {
      fighter,
      missile,
      blast
   };

   enum Images : TextureId
   {
      background,
   };

   enum Fonts : FontId
   {
      consolas,
   };

   enum Teams : TeamId
   {
      red,
      blue
   };
}

Hunt::Hunt(
   std::vector<neural::Network>::const_iterator begin,
   std::vector<neural::Network>::const_iterator end, 
   bool gui)
   : m_entityManager()
   , m_systemManager()
   , m_eventService()
   , m_spriteCache()
   , m_textureCache()
   , m_textCache()
   , m_teamColors()
   , m_hunterIds()
   , m_gui(gui)
   , m_maxGameDuration(10'000)
{
   //hunter start pos
   math::Vector2D const hunterStartPos(-100.0f, 0.0f);
   float const hunterStartTheta = 0.0f;

   //target start pos
   math::Vector2D const targetStartPos(0.0f, 0.0f);
   float const targetStartTheta = -math::pi() / 4;

   //graphics
   if (m_gui)
   {
      m_spriteCache.add(Sprites::fighter, "media/fighter-jet.png", 0.0f, 0.02f);
      m_spriteCache.add(Sprites::missile, "media/missile-512.png", -math::pi() / 4.0f, 0.03f);
      m_spriteCache.add(Sprites::blast, "media/explosion.png", 0.0f, 0.1f);
      
      m_textureCache.add(Images::background, "media/pattern2.png");

      m_textCache.add(consolas, "media/consolas.ttf");
   }

   //TODO: Need to sort out this dependency, it is not good that the Duel header requires sf::Color
   m_teamColors[Teams::red] = sf::Color(0xff, 0x7f, 0x00);
   m_teamColors[Teams::blue] = sf::Color(0x00, 0x7f, 0xff);

   //systems
   m_systemManager.addSystem<Tick>(m_entityManager);
   m_systemManager.addSystem<UpdateBrains>(m_entityManager);
   m_systemManager.addSystem<UpdateSineControl>(m_entityManager);
   m_systemManager.addSystem<UpdateFireControls>(m_entityManager, Sprites::missile, Fonts::consolas);
   m_systemManager.addSystem<UpdateFixedWings>(m_entityManager);
   m_systemManager.addSystem<UpdateCollisions>(m_entityManager);
   m_systemManager.addSystem<UpdateTickLimits>(m_entityManager);
   m_systemManager.addSystem<UpdateWarheads>(m_entityManager, Sprites::blast);
   m_systemManager.addSystem<UpdateScore>(m_entityManager, m_eventService);
   if (m_gui)
   {
      auto spWindow = std::make_shared<::sf::RenderWindow>(::sf::VideoMode(1920, 1080), "Evosim", ::sf::Style::Default/*, ::sf::ContextSettings(24U, 8U, 16U)*/);
      m_systemManager.addSystem<ClearWindow>(spWindow);
      m_systemManager.addSystem<ProcessWindowEvents>(spWindow);
      m_systemManager.addSystem<Render>(m_entityManager, spWindow, m_spriteCache, m_textureCache, m_textCache, m_teamColors);
      m_systemManager.addSystem<RenderHud>(m_entityManager, spWindow, m_textCache);
      m_systemManager.addSystem<FlushWindow>(spWindow);
   }
   m_systemManager.addSystem<CheckGameOver>(m_entityManager, m_eventService, m_maxGameDuration);
   m_systemManager.addSystem<Cleanup>(m_entityManager);

   //admin
   auto admin = m_entityManager.addEntity();
   admin.addComponent<TickCounter>();

   //background
   auto background = m_entityManager.addEntity();
   background.addComponent<Transform>();
   background.addComponent<Background>(Images::background);

   //scoreboard
   auto scoreboard = m_entityManager.addEntity();
   scoreboard.addComponent<ScoreBoard>(Fonts::consolas);

   //target
   auto target = m_entityManager.addEntity();
   target.addComponent<Transform>(targetStartPos, 0.0f, targetStartTheta);
   target.addComponent<Collision>();
   target.addComponent<FixedWingBody>()->m_minSpeed *= 2.0f;
   target.addComponent<FixedWingControls>();
   target.addComponent<SineControl>();
   target.addComponent<Sprite>(Sprites::fighter);
   target.addComponent<Team>(Teams::red);
   target.addComponent<ScreenFocus>();

   //hunters
   for(auto itr = begin; itr != end; itr++)
   {
      auto hunter = m_entityManager.addEntity();
      m_hunterIds.push_back(hunter.id());
      hunter.addComponent<Transform>(hunterStartPos, 0.0f, hunterStartTheta);
      hunter.addComponent<FixedWingBody>();
      hunter.addComponent<FixedWingControls>();
      hunter.addComponent<FireControls>();
      hunter.addComponent<Sprite>(Sprites::fighter);
      hunter.addComponent<Label>(itr->m_name, Fonts::consolas);
      hunter.addComponent<Brain>(*itr);
      hunter.addComponent<Score>();
      hunter.addComponent<Team>(Teams::blue);
      hunter.addComponent<Nemesis>(target.id());
   }
}

std::vector<float> Hunt::run()
{
   GameEventListener listener(m_eventService);

   for (;;)
   {
      if (m_gui)
      {
         std::this_thread::sleep_for(std::chrono::microseconds(4'000));
      }

      m_systemManager.update();

      if (listener.gameOver())
      {
         std::vector<float> scores;
         for (auto&& hunterId : m_hunterIds)
         {
            scores.push_back(score(hunterId));
         }
         return scores;
      }
   }
}

//More is better
float Hunt::score(framework::EntityId hunterId)
{
   auto pScore = m_entityManager.componentIndex().getComponent<Score>(hunterId);

   //some weights?
   static float const distanceWeight = -0.001f;
   static float const hitWeight = 1000.0f;
   static float const missWeight = -2000.0f;

   //Everything normalized to game time to avoid re-scaling issues
   float const distanceScore = distanceWeight * pScore->m_accumulatedDistanceToNemesis / m_maxGameDuration;
   float const hitScore = hitWeight * pScore->m_numHits / m_maxGameDuration;
   float const missScore = missWeight * pScore->m_numMisses / m_maxGameDuration;

   return distanceScore + hitScore + missScore;
}