#pragma once

#include "diversity/neural/scored_network.h"

namespace evosim
{
   namespace hunt
   {
      class Matchmaker
      {
      public:
         std::vector<::diversity::neural::ScoredNetwork> evaluate(
            std::vector<::diversity::neural::Network>&& networks,
            size_t numNetworksPerChase,
            size_t numThreads);
      };
   }
}