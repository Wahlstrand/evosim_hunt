#include <iostream>
#include <sstream>
#include <chrono>
#include <thread>
#include <iomanip>

#include "diversity/neural/network_breeder.h"
#include "diversity/neural/network_writer.h"
#include "diversity/neural/downselector.h"

#include "evosim/hunt/matchmaker.h"

using namespace ::diversity;
using namespace ::evosim;
using namespace ::evosim::hunt;

int main(int /*argc*/, char** /*argv*/)
{
   try
   {
      size_t const numShootersPerRun = 20;
      size_t const numThreads = std::thread::hardware_concurrency();

      size_t const numElites = 20;     //number of surviving networks
      size_t const reproductions = 1;  //each elite will breed with each other elite this many times, 
                                       //creating this many times n*(n-1)/2 purely genetical children
      size_t const mutations = 5;      //each elite will create this 4x many mutations of itself

      neural::NetworkBreeder breeder({ 3, 5, 5, 2 });
      neural::NetworkWriter writer;
      Matchmaker matchmaker;
      neural::Downselector downselector;

      auto networks = breeder.initial(numElites);
      for(size_t generation = 0;;generation++)
      {
         auto evolutions = breeder.evolve(networks, reproductions, mutations);
         auto scoredNetworks = matchmaker.evaluate(std::move(evolutions), numShootersPerRun, numThreads);

         auto newElites = downselector.downselect(scoredNetworks, numElites);

         if (generation % 1 == 0)
         {
            std::stringstream ss;
            ss << generation << ".xml";
            writer.write(newElites[0].m_network, ss.str());
         }
        
         std::cout << "Generation " << std::setw(5) << generation << " running (size " << evolutions.size() << ") -> best score is " << newElites[0].m_score<< std::endl;
         networks = stripScore(std::move(newElites));
      }
   }
   catch (std::exception const& e)
   {
      std::cout << e.what();
      getchar();
   }
}
