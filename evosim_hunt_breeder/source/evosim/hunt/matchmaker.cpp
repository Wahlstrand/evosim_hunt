#include "evosim/hunt/matchmaker.h"

#include <thread>
#include <future>
#include <iostream>

#include "diversity/math/math.h"

#include "evosim/hunt/hunt.h"

using namespace ::diversity;
using namespace ::evosim;
using namespace ::evosim::hunt;

namespace
{
   struct Match
   {
      Match(
         size_t firstnetworkIdx,
         size_t lastNetworkIdx)
         : m_firstNetworkIdx(firstnetworkIdx)
         , m_lastNetworkIdx(lastNetworkIdx)
      {
      }
      size_t m_firstNetworkIdx;
      size_t m_lastNetworkIdx;
      std::vector<float> m_score;
   };

   class MatchKeeper
   {
   public:
      MatchKeeper(std::vector<Match>&& matches)
         : m_matches(std::move(matches))
         , m_next(m_matches.begin())
      {
      }
   
      Match* takeMatch()
      {
         std::lock_guard<std::mutex> lock(m_mutex);

         //std::cout << std::this_thread::get_id() << " took " << std::distance(m_matches.begin(), m_next) << std::endl;

         if (m_next == m_matches.end())
         {
            return nullptr;
         }
         
         return &*m_next++; //dat syntax
      }

      std::vector<float> score() const
      {
         std::vector<float> result;
         for (auto&& match : m_matches)
         {
            if (result.size() < match.m_lastNetworkIdx + 1)
            {
               result.resize(match.m_lastNetworkIdx + 1);
            }

            for (size_t n = 0; n < match.m_score.size(); n++)
            {
               result[match.m_firstNetworkIdx + n] += match.m_score[n];
            }
         }
         return result;
      }

   private:
      std::vector<Match> m_matches;
      std::vector<Match>::iterator m_next;
      std::mutex m_mutex;
   };
   
   class ChaseRunner
   {
   public:
      ChaseRunner(MatchKeeper& matchKeeper, std::vector<::neural::Network> const& networks)
         : m_matchKeeper(matchKeeper)
         , m_networks(networks)
      {
      }

      void run()
      {
         while (auto pMatch = m_matchKeeper.takeMatch())
         {
            Hunt hunt(
               std::next(m_networks.begin(), pMatch->m_firstNetworkIdx),
               std::next(m_networks.begin(), pMatch->m_lastNetworkIdx + 1),
               false);
            pMatch->m_score = hunt.run();            
         }
      }
   
   private:
      MatchKeeper& m_matchKeeper;
      std::vector<::neural::Network> const& m_networks;
   };

   std::vector<Match> makeMatches(
      std::vector<::neural::Network> const& networks, 
      size_t numNetworksPerChase)
   {
      std::vector<Match> result;
      for (size_t first = 0; first < networks.size(); first += numNetworksPerChase)
      {
         size_t last = std::min(first + numNetworksPerChase, networks.size()) - 1;

         result.emplace_back(first, last);
      }
      return result;
   }
}


std::vector<neural::ScoredNetwork> Matchmaker::evaluate(
   std::vector<::neural::Network>&& networks,
   size_t numNetworksPerChase,
   size_t numThreads)
{
   MatchKeeper matchKeeper(makeMatches(networks, numNetworksPerChase));

   std::vector<ChaseRunner> runners(numThreads, { matchKeeper, networks });
   std::vector<std::thread> threads;
   for (auto&& runner : runners)
   {
      threads.emplace_back(&ChaseRunner::run, &runner);
   }

   for (auto&& thread : threads)
   {
      thread.join();
   }
   
   return neural::addScore(
      std::move(networks),
      matchKeeper.score());
}