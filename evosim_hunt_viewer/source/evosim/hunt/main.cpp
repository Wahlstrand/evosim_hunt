#include <iostream>

#include "diversity/math/math.h"

#include "diversity/neural/network_reader.h"

#include "evosim/hunt/hunt.h"

using namespace ::diversity;
using namespace ::evosim;

int main(int argc, char** argv)
{
   try
   {
      std::vector<neural::Network> networks;

      neural::NetworkReader reader;
      for (int argIdx = 1; argIdx < argc; argIdx++) //first arg is the executable
      {
         networks.emplace_back(reader.read(argv[argIdx]));
      }

      hunt::Hunt hunt(
         networks.begin(),
         networks.end(), 
         true);
      hunt.run();

      getchar();
   }
   catch (std::exception const& e)
   {
      std::cout << e.what();
      getchar();
   }
}
